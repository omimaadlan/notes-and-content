## Method Practice

Write a method `isEmail` which returns true if the input String contains an @ symbol and a period. It should return false otherwise.

Write a method `printSpelling` that iterates through a given String and prints out each letter to the console on a new line.

Write a method `letterCount` that takes in two parameters - a char, and a String - and returns the number of times the given character is found in the String.

Write a method `reverseString` that takes in a String and returns the reversed value. Try this with String and also with StringBuilder.

Write a method `concat` that takes in a String and a number, and returns that string concatenated that number of times (`concat("hello",4)` yields "hellohellohellohello"). Use a StringBuilder to do so.