Some git commands used today:

- `git init`
- `git status` 
- `git add [filename]`
- `git add` 
- `git commit -m "commit message"`
- `git log`
- `git push -u origin main`
- `git push`
- `git remote -v` 
- `git remote add origin [url.git]`
- `git clone`

*note: When git log has lengthy output, or when you run the commit command without the -m tag, git uses vim and begins in insert mode. To exit, enter `:q`. If you want to leave a commit message, you'll need to enter insert mode by entering `i`, typing your message, and then leaving insert mode by pressing `esc`. Then `:wq` will write the message and quit. 

Other resources:
- git [cheat sheet](https://education.github.com/git-cheat-sheet-education.pdf) 
- git [cli documentation](https://git-scm.com/docs)