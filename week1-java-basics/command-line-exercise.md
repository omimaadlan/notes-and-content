## Familiarizing Yourself with Working in the Command Line

Today, we'll get some practice navigating our file system and performing simple operations in the command line. Use Git Bash for each prompt, and do your best to execute the command on your own. If you need some help, click on the question's arrow to see a solution. 

0. Check who the logged in user is.

<details>
    <summary>Answer</summary>
    <code>whoami</code>
</details>

1. Navigate to the root directory of your machine.

<details>
    <summary>Answer</summary>
    <code>cd /</code>
</details>

2. Display all of the files in your root directory.

<details>
    <summary>Answer</summary>
    <code>ls</code>
</details>

3. Now navigate to your home directory.

<details>
    <summary>Answer</summary>
    <code>cd ~</code>
</details>

4. Make a new directory in your home directory called "test-project".

<details>
    <summary>Answer</summary>
    <code>mkdir test-project</code>
</details>

5. Without navigating out of your home directory, display the contents of the test-project folder.

<details>
    <summary>Answer</summary>
    <code>ls test-project</code>
</details>

6. Create a new file ("index.txt") in the test-project folder.

<details>
    <summary>Answer</summary>
    <code>touch test-project/index.txt</code> or <code>cd test-project</code> followed by <code>touch index.txt</code>
</details>

7. Now display the contents of the test-project folder. Include other file details like the owner and timestamp.

<details>
    <summary>Answer</summary>
    <code>ls -l</code>
</details>

8. Add a few lines (3+) of code to the index.txt file.

<details>
    <summary>Answer</summary>
    Use vim or nano for this. You can also redirect output into a file. The 'echo' command outputs its given argument, and the ">" operator redirects output into a file. ">" overwrites the file, while ">>" appends to it.  <code>echo 'line 1' > index.txt</code> <code>echo 'line 2' >> index.txt</code> <code>echo 'line 3' >> index.txt</code>
</details>

9. Print the contents of that file.

<details>
    <summary>Answer</summary>
    <code>cat index.txt</code>
</details>

10. Print the first two lines of index.txt.

<details>
    <summary>Answer</summary>
    <code>head -2 index.txt</code>
</details>

11. Print the last two lines of index.txt.

<details>
    <summary>Answer</summary>
    <code>tail -2 index.txt</code>
</details>

12. Make a copy of index.txt, called index-copy.txt

<details>
    <summary>Answer</summary>
    <code>cp index.txt index-copy.txt</code>
</details>

13. Delete the original index.txt file.

<details>
    <summary>Answer</summary>
    <code>rm index.txt</code>
</details>

14. Rename index-copy.txt to index.txt.

<details>
    <summary>Answer</summary>
    <code>mv index-copy.txt index.txt</code>
</details>

15. Delete the test-project folder, along with the file in it.

<details>
    <summary>Answer</summary>
    <code>cd ..</code> followed by <code>rm -r test-project</code> 
</details>


