## Collection Practice Exercises

---
### Sets 
---

Use IntelliJ along with Java’s [documentation](https://docs.oracle.com/javase/8/docs/api/) and Java's [source code](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/tip/src/share/classes/java/util) to explore the following ideas.

1.	HashSet 
    - Create a HashSet with a User object (userId, name, email, password, getters, setters, toString, equals, hashcode). Add some Users.
    - What do you notice about the HashSet methods relative to the Collection interface's methods?
    - Print each Product. What is the order determined by?
    - Take a look at the [source code](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/HashSet.java) for HashSet. What is the “PRESENT” variable?
2.	TreeSet
    - Create a TreeSet with Product objects. Add some products.
        - What issue do you expect to encounter?
    - Create a comparator, sorting by productId. Add it to the constructor of the TreeSet. 
        - Why does this solve the previous issue?
        - Can you add null values? Why or why not?
    - What do you notice about the TreeSet methods relative to a HashSet?
    - What happens if you add a Product with the same productId but different quantity and price?
    - Iterate through the set and print each Product. What is the order determined by?
    - Take a look at the [source code](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/TreeSet.java) for TreeSet here. What does the m variable represent? How are the implementation of the HashSet and TreeSet similar?


--- 
### Lists
---

1.	ArrayList
    - Create a new ArrayList with no generic type 
    - Add several strings
        - If the position isn’t specified, where is the element added?
        - What is the difference between the set method and the add method when an index is provided?
    - Retrieve the 0th index of the ArrayList and save it to a variable of type int.
        - Will this compile? Why or why not?
    - Cast the object so that it compiles and run the program.
        - Discuss the issue encountered at runtime.
    - Add a generic to your ArrayList so that it only accommodates Strings. Add some strings to the ArrayList.
    - Iterate through the ArrayList, printing each value in the list using the get(int index) method. 
        - Where is this method implemented? Where is it originally specified? (Which class or interface)
        - What do you notice about the order of the Strings?
    - Remove an element from the ArrayList.
        - What is the difference between the remove methods?
    - Take a look at the implementation [here](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/ArrayList.java). 
2. LinkedList
    - Take a look at the implementation of the linked list [here](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/LinkedList.java). Note the instance variables declared after the class declaration, as well as the [node class](http://hg.openjdk.java.net/jdk8/jdk8/jdk/file/687fd7c7986d/src/share/classes/java/util/LinkedList.java#l970).

### Collections Utility Class

1.	LinkedList
    - Create a new LinkedList of Integers, add several Integers.
    - Using methods from the Collections utility class:
        - Sort the LinkedList in increasing numeric order
        - Sort the method again with a Comparator to sort the LinkedList in decreasing numeric order
        - Randomly permute the list
        - Reverse the list
        - Find the lowest Integer value
        - Find the highest Integer value - use the same method to do this, using the Comparator from (e.) will make this possible
        - Search for a particular element in the list

---
### Queues
---

1.	LinkedList - use the linked list from the previous section
    - What methods does LinkedList share with ArrayList?
    - What methods does it have from Queue?
    - Use the Queue methods to add and remove elements in FIFO order.
    - What methods does it have that are specified in Deque? 
        - Compare the methods from Deque to those in Queue?
    - Use Deque methods to treat the LinkedList like a stack. Adding and removing elements in a LIFO order.


