### Array Exercises:
- create an array of 3 strings
- print out the second value in the array
- print out the eighth character of the first string
    - what happens if your first string has less than eight characters? 
    - how would you avoid that issue?
- add a fourth string to the array
    - can this be done directly? 
    - how can we accomplish this?
- define a method that can take any number of string variables and prints each provided string on a new line (using var args)

### Exceptions Exercises:
- Define an int x, with any value. Then define an int y, and assign it to 0. Divide x by y. What happens?
    - Our code still compiled even though our code risked throwing an Exception. What kind of exception was thrown?
- Wrap this code in a try/catch block. Have your program print "cannot divide by zero," if the Exception is thrown. What type of Exception should you define in the catch block? 
- Is there anyway we could have prevented the Exception from being thrown? Replace the try/catch block with logic that assures that the Exception will never be thrown.

- Create a method "addPositiveNums" which takes in two int parameters and returns their sum.
    - Throw a RuntimeException if either of the numbers are less than 0.
    - Call addPositiveNums with valid positive input in the main method. What happened? 
    - Call addPositiveNums with invalid negative input. What happens?
- Change the RuntimeException to just an Exception. How does that change your code?
    - Add a throws clause to to the addPositiveNums method so that the method compiles. Does this introduce any problems elsewhere?
- Wrap the method call to addPositiveNums in the main method in a try/catch block.
    - Call addPositiveNums with valid positive input in the main method. What happened? 
    - Call addPositiveNums with invalid negative input. What happens?
        - When does your addPositiveNums actually throw the Exception? Can it determine that an Exception is thrown at compile time? Or is the Exception thrown when the program runs, based on the method's parameters?
- If someone else was using your addPositiveNums method, could they reasonably prevent the Exception from being thrown? Is there any logic that would assure that the Exception never be thrown?
    - We can put a checked or an unchecked Exception in this method. Which would be a better fit?
