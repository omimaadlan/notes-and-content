/*
 *  DDL used to create a table in our db
 */
create table product(
	id integer primary key, -- primary key = unique non null
	name varchar(60) not null,
	price numeric(5,2) check (price>0) -- up to 999.99
);

-- truncate is another DDL keyword
truncate table product;

/*
 * DML used to add records to our table
 */
insert into product values (1,'shoes',40.00);
insert into product values (2,'pants',32.00);
insert into product values (3,'shirt',15.50);
insert into product values (4, 'socks', 3.00);
insert into product values (5, 'dress',35.00);

--begin;
--insert into product values (5, 'dress',35.00);
--commit;

/*
 * DQL used to query records in a a table
 */
select *
from product;

select name 
from product;

select * -- select all columns 
from product 
where name = 'shoes';

select *
from product
where price < 20;

select *
from product
order by price desc;


/*
 * Exploring more DML keywords
 */
update product 
set price = 5
where id = 4;

delete from product 
where id = 2;

update product 
set price = 38
where id = 5;


