## SQL Practice

Before doing these exercises, you can run the [product-department-setup.sql](./sql-scripts/product-department-setup.sql) file. This will create the schema we worked with if it doesn't exist, or reset it if it does exist. Open it in your file system, and select the "Execute SQL Script" option, rather than the "Execute SQL Statement" option. There is a small script icon below the yellow play button for this.

### Practice with DDL

1. create a table "sales_representative"
    - the sales rep table should have an auto-incrementing primary key
    - each sales rep should have a first and last name
    - each sales rep should be associated with a particular department

    <details>
        <summary>Possible solution</summary>

        
        create table sales_representative(
            rep_id serial primary key,
            first varchar(50),
            last varchar(50),
            department int references department
        )
        

    </details>


2. add a new column to the sales_representative table to give each sales rep an email

    <details>
        <summary>Possible solution</summary>

        
        drop table sales_representative;
        

    </details>


3. drop the sales_representative table

    <details>
        <summary>Possible solution</summary>

        
        alter table sales_representative 
        add column email varchar(100) not null;        
        

    </details>


4. recreate the table, this time include the email column definition when you create the table 

    <details>
        <summary>Possible solution</summary>

        
        create table sales_representative(
            rep_id serial primary key,
            first varchar(50),
            last varchar(50),
            email varchar(100) not null,
            department int references department
        )       
        

    </details>


### Practice with DML

1. Add a few records to the sales_representative table.
    - Each should reference a valid department.

    <details>
        <summary>Possible solution</summary>

        
        insert into sales_representative (first, last, email, department) values ('Sally', 'Jenkins', 'sjenkins@gmail.com', 2);
        insert into sales_representative (first, last, email, department) values ('Tommy', 'Pickles', 'tpicks@gmail.com', 1);
        insert into sales_representative (first, last, email, department) values ('Julie', 'Smith', 'jsmith27@gmail.com', 1);
        insert into sales_representative (first, last, email, department) values ('Frank', 'Daniels', 'frankied@gmail.com', 2);      
        

    </details>


    > Briefly back to DDL: truncate the table. What happens? 
    > Repopulate the table. 

2. Imagine one of our sales_representative has a new email, change the email field of a sales representative.

    <details>
        <summary>Possible solution</summary>

        
        update sales_representative 
        set email = 'julie.smith@hotmail.com'
        where rep_id = 3;
        

    </details>

3. Imagine one of our sales_representative has gotten married and changed their last name and their email to reflect it. Change both the email field and the last name field of the sales representative (use one SQL statement to change both fields).

    <details>
        <summary>Possible solution</summary>

        
        update sales_representative 
        set last = 'Gherkin', email = 'sgherkin@hotmail.com'
        where rep_id = 1;
        

    </details>

4. Delete sales_representative with id 2.

    <details>
        <summary>Possible solution</summary>

        
        delete from sales_representative
        where rep_id = 2;
        

    </details>

### Practice with DQL

1. Select all sales representatives from the table.
    - order the results by last name (A-Z)

    <details>
        <summary>Possible solution</summary>

        
        select *
        from sales_representative
        order by last;
        

    </details>

2. Select all of the products where the product name is 'shirt'

    <details>
        <summary>Possible solution</summary>

        
        select * 
        from product 
        where name = 'shirt';
        

    </details>

3. Select all of the sales representatives alongside their corresponding department.
    - First, show all columns from both tables.
    - Then only show the employee's first name, last name, and department name. 

    <details>
        <summary>Possible solution</summary>

        
        select * 
        from sales_representative sr 
        left join department d 
        on sr.department = d.id;
        ---
        select sr.first, sr.last, d.name 
        from sales_representative sr 
        left join department d 
        on sr.department = d.id;
        

    </details>

4. Show all of the products that cost more than 5.00, along with the department they belong in.

    <details>
        <summary>Possible solution</summary>

        
        select *
        from product p 
        left join department d 
        on p.department = d.id
        where p.price > 5;
        

    </details>

4. (challenge question!) Show all of the products with their corresponding department, and all of the corresponding sales reps for that department.

    <details>
        <summary>Possible solution</summary>

        
        select *
        from product p 
        left join department d 
        on p.department = d.id
        left join sales_representative sr 
        on sr.department = d.id;
        

    </details>