# SQL 
- SQL = Structured Query Language
- a language which provides a means for interacting with a database 

### Relational Databases

- table based architecture, with columns of the tables relating to one another (FK relationships)
- when you define a table, you define the data type of the column, but you can also place additional restrictions on the data using constraints (fk, pk, unique, not null, check)
- data integrity - changes to db follow specifications of the db (types, constraints)
- referential integrity - changes to db maintain relationships specified in our db (can't delete records being referenced, cannot reference an entity or record that doesn't exist, etc)

1. Candidate Key
    - any column or combination of columns which can uniquely identify a row 
2. Primary Key
    - the column or combination of columns which acts as the unique identifier for each record
    - must be unique
    - cannot be null
    - the primary key can be defined as naturally occurring data (e.g. SSN, email) - this is called a natural key
    - the primary key can also be defined using a more arbitrary value, created solely to identify the record in the database - this is called a surrogate key
3. Foreign Key
    - a column which references a value in another table
    > invoice and customer  

## Data Types

When we define each column in a RDB, we define the type of data we expect that column to hold.  For instance, if we have a column for someone's email, we expect that column to hold characters (`varchar`). If we want to store whether or not an account is active, we can have that column store a boolean. In many cases, data types in our db will look slightly different than Java data types.

-   boolean: evaluates to true or false
-   character (`char[n]`) : a fixed length character string
-   character varying (`varchar[n]`) : a varying length string of characters
-   date : a calendar date
-   integer: a whole number
-   numeric/decimal: a decimal number

PostgreSQL [documentation](https://www.postgresql.org/docs/13/datatype.html) on data types.

## Schema

A database schema refers to the formal structure of data defined by a relational database, this includes:
- the various tables in the database and their columns and data types
- the relationships between tables in the database

### Constraints

Constraints are restrictions that we can put on the columns of our tables in SQL

Some constraints in SQL include:

-   Composite keys: a key that uses more than one column
-   Not null: the column must have data
-   Unique: no two records entered into this column can be the same
-   Primary key: denotes that this column is a primary key
-   Foreign key: denotes that this column is pointing to an attribute on another table
-   Default: creates a default value if now is given

# SQL and its sub-languages
- there are many different operations we can perform on our db
- these operations can be broken down into types of operations or sub-languags within SQL

### DDL - Data Definition Language
- used to define the structure of our database entities
- any table scoped operations fall under DDL
- `create`, `drop`, `truncate`, `alter`
- note: when defining tables, we specify the [data type](https://www.postgresql.org/docs/9.5/datatype.html) held in each column, as well as defining any [constraints](https://www.postgresql.org/docs/9.4/ddl-constraints.html) we would like placed - constraints can be placed on a single column, or on a table using multiple columns

### DML - Data Manipulation Language 
- represents the operations we do on the records themselves
- CRUD operations (Create Read Update Delete); the main operations we do on data
- `insert`, `update`, `delete`, `select`
    - `select` is sometimes considered part of its own sublanguage, DQL, or Data Query Language

#### Query clauses

select [columns]

from [table]

where [condition] -- filters pre aggregation if aggregation is occurring

group by [column] -- defines how our data is going to be aggregated

having [condition] -- filters after aggregation

order by [column][asc/desc]

### TCL - Transaction Control Language
**Transaction:** a unit of work done on a database - it consists of one or more related operations executed as a unit
- TCL helps facilitate creating explicit transactions to group together related operations
- `begin`,`commit`, `rollback`, `savepoint`

#### Properties of a Transaction

**Atomicity**

- transactions executes in its entirety or not at all

**Consistency**

- db is in a consistent state before and after a transaction executes
- transaction maintains data and referential integrity

**Isolation**

- concurrent transactions should not affect the execution of one another

**Durable**

- committed data is permanent and cannot be rolled back


### DCL - Data Control Language
- allows you to manage user permissions on a database 
- `grant` `revoke`


---

## Multiplicity

- One to One: profile to login credentials, passport and travelers \*passport references traveler
- One to Many(/Many to One): role to user \*role references user id
- Many to Many: sales to products, favorite an item \*both tables are referenced in a third table (join table/ junction table)

## Joins

- allows us to create a single query which spans across multiple tables in our db
- there are various different types of joins, based on what information you would like from each table, and how you define the tables to be joined together
- the typical join syntax is a follows:
- inner, left (outer), right (outer), full (outer)

```sql
select [columns]
from [left table]
left/right/full join [right table]
on [join predicate];
```

There are various different types of joins, based on what information you would like from each table, and how you define a join predicate. A *theta* join is just a join which joins two tables based on some condition (defined above as the join predicate). An *equi* join, is a theta join, where that condition uses equality. 

```sql 
select employee.name, department.name
from employee
left join department
on department.id = employee.department;
```

We also have such joins as *natural* joins. These joins are implicit and do not need a join predicate. Instead, the join is performed based on columns with the same names. In the employee/department example above, if the department table had an id column with the name 'dept_id' and the employee table had a column with the name 'dept_id' which referred to the department table, a natural join could be performed.

```sql 
select employee.name, department.name
from employee
natural join department
```

## Index
An index is a common way to enhance database performance, enabling faster retrieval. When creating an index, the database stores an in memory ordering of a particular column. Without an index, if we were searching on a particular column, we would need to check every single value in that column to return the result. Indexes allow those operations to be more efficient. Columns which are searched on frequently, and that have a high percent of unique values and a low percent of null values are generally good candidates for an index. While indexes can make searching operations more efficient, we need to be careful not to use indexes too liberally. Because we are storing an additional ordering in memory, they need to be maintained every time we perform operations like insertions and deletions, so they may not always be the best choice.

```sql
CREATE INDEX index_name ON table_name (col_name);
```

## Views
There are two types of views in Postgres which allow us to view a dataset from a query. A traditional *view* will store a query in memory. Each time we make a request to that view, the query is executed and the result set is retrieved from the database.  There is also such thing as a *materialized view.* This type of view stores the actual dataset in memory. Each time we make a request to a materialized view, it does not execute the query again, but rather returns the saved dataset. To update the dataset, the materialized view must be refreshed.

```sql
create [materialized] view as [query]
```

## Normalization

- the process of reducing redundancy from relations in your db
- data is stored more efficiently, taking up less memory in your db
- when redundancy is present in your db, update and deletion anomalies can arise from having replicated data across your db; normalization prevents these anomalies

### 1. First Normal Form

- table has a primary key
- no repeated columns
- columns hold atomic values

> Note 'atomic' here is being used to say that values cannot be broken down any further. A full name for example, is not atomic; instead, we should break a full name column into a first and last name. This is a similar concept, but a different application to the way we understand 'atomicity' with transactions.

### 2. Second Normal Form

- satisfy criteria for first normal form
- remove partial dependencies

> Partial dependencies can only occur when a table has a composite key. If a database has no composite keys, it will have no partial dependencies. Thus, if a database is in the first normal form and has no composite keys, the database is by default in the second normal form. If the database does have a composite key, partial dependencies exist where another column depends only on part of that composite key. 

### 3. Third Normal Form

- satisfy criteria for second normal form
- remove transitive dependencies

> Transitive dependencies occur when one column can be derived from one or more other columns. City and state could be considered a transitive dependency of a zip code field. A pre-tax subtotal and post-tax total need not both be stored in the database if the tax value is known and can be calculated.

## Example
<img src="./note-images/invoice.png" alt="invoice data">

We want to represent the above invoice data in our db. We'll start with one table, an invoice table, which includes every piece of data from two invoices.

**invoice**

| invoice_id | customer_name | date        | item       | price | quantity | line price | total |
| ---------- | ------------- | ----------- | ---------- | ----- | -------- | ---------- | ----- |
| 201        | Peter         | 2 June 2020 | Coffee     | 5.00  | 2        | 10.00      | 25.00 |
| 201        | Peter         | 2 June 2020 | Milk       | 3.00  | 1        | 3.00       | 25.00 |
| 201        | Peter         | 2 June 2020 | Bread      | 4.00  | 3        | 12.00      | 25.00 |
| 204        | Patrick       | 3 June 2020 | Cereal     | 3.50  | 3        | 10.50      | 29.25 |
| 204        | Patrick       | 3 June 2020 | Wine       | 7.00  | 2        | 14.00      | 29.25 |
| 204        | Patrick       | 3 June 2020 | Watermelon | 4.75  | 1        | 4.75       | 29.25 |

This table is not yet normalized. You can already see there is quite a bit of redundancy. We also do not yet have a primary key, so this table is not yet even in the first normal form.

Do we have any column(s) that are a good candidate for a primary key? Do any fields uniquely identify each row? 

<details>
    <summary>See more</summary>
    None by themselves. We may need to include some more information to uniquely identify each row.
</details>

---

**invoice**

| _invoice_id_ | customer_name | date        | _line_number_ | item       | price | quantity | line price | total |
| ------------ | ------------- | ----------- | ------------- | ---------- | ----- | -------- | ---------- | ----- |
| 201          | Peter         | 2 June 2020 | 1             | Coffee     | 5.00  | 2        | 10.00      | 25.00 |
| 201          | Peter         | 2 June 2020 | 2             | Milk       | 3.00  | 1        | 3.00       | 25.00 |
| 201          | Peter         | 2 June 2020 | 3             | Bread      | 4.00  | 3        | 12.00      | 25.00 |
| 204          | Patrick       | 3 June 2020 | 1             | Cereal     | 3.50  | 3        | 10.50      | 29.25 |
| 204          | Patrick       | 3 June 2020 | 2             | Wine       | 7.00  | 2        | 14.00      | 29.25 |
| 204          | Patrick       | 3 June 2020 | 3             | Watermelon | 4.75  | 1        | 4.75       | 29.25 |

We've introduced a new field, line number. "line_number" and "invoice_id" together, uniquely identify each row. The two together become a composite primary key.

We are now in first normal form. 

But are we in second normal form? In order to be in second normal form, we must remove any partial dependencies. Are there any columns which rely on only part of the primary key? Any fields that depend on only the line number, or only the invoice id?

<details>
    <summary>See more</summary>
    Customer name, date, and total, all depend only on the invoice id. There is no correlation between these fields and the line id. Because of this, these three fields are partially dependent on the primary key. We'll have to remove these partial dependencies to be in second normal form.
</details>

---

invoice_line

| _invoice_id_ | _line_number_ | item       | price | quantity | line price |
| ------------ | ------------- | ---------- | ----- | -------- | ---------- |
| 201          | 1             | Coffee     | 5.00  | 2        | 10.00      |
| 201          | 2             | Milk       | 3.00  | 1        | 3.00       |
| 201          | 3             | Bread      | 4.00  | 3        | 12.00      |
| 204          | 1             | Cereal     | 3.50  | 3        | 10.50      |
| 204          | 2             | Wine       | 7.00  | 2        | 14.00      |
| 204          | 3             | Watermelon | 4.75  | 1        | 4.75       |

invoice

| _invoice_id_ | customer_name | date        | total |
| ------------ | ------------- | ----------- | ----- |
| 201          | Peter         | 2 June 2020 | 25.00 |
| 204          | Patrick       | 3 June 2020 | 29.25 |

Now we've removed partial dependencies, by creating a separate table for the information that relies only on the invoice id. The invoice line table still has a composite key, but all of the fields are dependent on both of those columns.

Now is this setup in the third normal form? We know in order to be in the third normal form, we need to remove any transitive dependencies. Are there any columns whose values can be derived from one or more other columns in the db?

<details>
    <summary>See more</summary>
    Right off the bat, we can see that line price is a derived value, and that it can easily be calculated by multiplying the item price by its quantity. This makes line total a transitive dependency. By the same logic, the invoice total, can also be calculated, even though that aggregation would be a little less straightforward. A little less obvious, we can see that price is a function of the item. We don't see a lot of redundancy in our data set, but we can imagine as this scales, each time an item is listed, the same price will be listed alongside it. We can move items and their prices into their own table to avoid any redundancy that could come from this. 
</details>

---

invoice_line

| _invoice_id_ | _line_number_ | item_no | quantity |
| ------------ | ------------- | ------- | -------- |
| 201          | 1             | 73627   | 2        |
| 201          | 2             | 76283   | 1        |
| 201          | 3             | 72463   | 3        |
| 204          | 1             | 84726   | 3        |
| 204          | 2             | 83725   | 2        |
| 204          | 3             | 82617   | 1        |

invoice

| _invoice_id_ | customer_name | date        |
| ------------ | ------------- | ----------- |
| 201          | Peter         | 2 June 2020 |
| 204          | Patrick       | 3 June 2020 |

item

| _item_no_ | name       | price |
| --------- | ---------- | ----- |
| 73627     | Coffee     | 5.00  |
| 76283     | Milk       | 3.00  |
| 72463     | Bread      | 4.00  |
| 84726     | Cereal     | 3.50  |
| 83725     | Wine       | 7.00  |
| 82617     | Watermelon | 4.75  | 

Now, our db is in third normal form.

While there are additional normal forms, a db being in third normal form is generally considered to be adequately normalized.

### Denormalization

While normalization is a well accepted standard with significant benefits, there are some arguments for denormalizing a database in certain instances. Let's say, the information you most often need from your database requires a lot of expensive operations - perhaps there are many aggregated values across many tables. If each time we need data we're doing many operationally intensive joins and calculations, then the cost of normalizing our data could be greater than the benefits. 