## Java Review Questions

### Week 1 
- What is an OS?
- What is the difference between a computer's primary and secondary storage?
- What are some common bash commands?
- What are environment variables?
- What is a command line text editor?
- What are some features and benefits of Java?
- What is the difference between a class and an object?
- What are the 4 pillars of OOP? Explain each. Give examples as they relate to Java specifically.
- What is the difference between the JDK, JRE, and JVM?
- How can I compile and execute Java code in the command line?
- What is the main method? What is the method signature?
- Describe the different parts of a method declaration.
- What is a package in Java? How do they benefit us as developers?
- What are import statements used for?
- What is a constructor?
- What is meant when we say that we cannot "break the constructor chain"?
- What is the super keyword used for?
- How can you define a constructor?
- What is the difference between a constructor and a method?
- What are wrapper classes? When are they used?
- What is git?
- What is the difference between git, gitlab, github, and git bash?
- What are some common git commands?
- Explain the git workflow of making a change locally and updating it remotely.
- What is a commit?
- What are git branches? Why might we want to create multiple branches?
- What is a pull request (or a merge request)?
- What are the access modifiers in Java?
- What are some non access modifiers?
- What is the static keyword?
- What does the final keyword indicate? Where can it be used?
- How are Strings different than other objects in Java?
- What are the differences between a String, StringBuilder, and StringBuffer?
- When would we want to cast objects? What limitations do we have when we are casting objects? How can we avoid running into any issues when casting?
- What are some control flow statements? Explain the use of each.

### Week 2 
- What is an array?
- What are the differences between an array and a collection?
- What is the object class?
- What are some of the common methods you'd find in the object class? 
- Which would you want to override for your own objects? How would you want to implement these methods?
- What are the different scopes of a variable in Java?
- Can I access a instance variable in a static method?
- Can I access a static variable in an instance method?
- What is the difference between when we say the scope of a variable and a variable's access modifier?
- What is an Exception in Java?
- What is the difference between an Exception and an Error?
- What are some examples of Errors?
- What are some examples of Exceptions?
- What is the difference between checked and unchecked Exceptions? What are some examples of each?
- Why might I want to define a custom Exception?
- How would I make a custom Exception? How would I define whether I wanted this Exception to be a checked or unchecked exception?
- What two things can I do to make code with a checked exception compile?
- Explain the use of a try block, a catch block, and a finally block.
- What is the use of a try with resources block? What _must_ be true about a resource if we want to declare it using one?
- What is TDD?
- What is unit testing?
- What is JUnit?
- How would you write a test using JUnit?
- What annotations are available with JUnit?
- What is an assert method? Examples?
- What is an interface?
- What is an abstract class?
- What are some differences between an interface and an abstract class? When would you use one over the other?
- What is the purpose of protecting a branch on github/gitlab? What kind of protection rules can I define?
- What is merging in git? How can we merge our code? What is a merge conflict?
- Describe the roles of stack and heap memory.
- What is garbage collection? 
- Do we have any control over when an object in heap memory is garbage collected?
- What is the Scanner class? What are some common methods used in the Scanner class?
- What is included in the Collection API in Java?
- What is the significance of the List interface?
- What is the significance of the Set interface?
- What is the significance of the Queue interface?
- What is the significance of the Map interface? How are Maps different from other data structures in Java?
- What is the significance of the Iterable interface? What is its role in the Collection hierarchy?
- What is an Iterator? When would we use an iterator rather than a for loop?
- What are generics? How are they useful to us as developers?
- How can you define a generic type?
- Can you provide primitive types as a generic?
- How can we define the way that objects are compared in Java?
- What is the difference between the Comparable and Comparator interfaces?
- Even if you don't intend to use the methods defined using the Comparable/Comparator yourself, where might these methods be used?
- HashSet vs TreeSet? What do they have in common? How are they different? When would you use one over the other?
- ArrayList vs LinkedList? What do they have in common? How are they different? When would you use one over the other?
- ArrayDeque vs PriorityQueue? What do they have in common? How are they different? When would you use one over the other?